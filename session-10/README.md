# 第10回

## Quartoを導入しよう

対話型ウインドウでpythonを実行できる（し，Rとかも実行できるし，ドキュメントも作れるし，論文も書ける）

- Get Started <https://quarto.org/docs/get-started/>
- Tutorial: Hello, Quarto <https://quarto.org/docs/get-started/hello/vscode.html>

`jupyter`がうまくインストールできないときには，その原因はファイルの書き込み権限かも。
管理者権限で`pip`を実行すると解決するかも。

> Python本体をC:¥Program Files (x86)等にインストールした場合に発生します。pip installではPython本体があるLib/site-packagesディレクトリにライブラリをインストールします。そのため、C:¥Program Files (x86)内のように管理者権限がないと書き込みできない場所にPython本体があるとエラーが発生します。

## Numpyキソ

まずは東大の資料

Pythonプログラミング入門（東京大学数値・情報教育研究センター） <https://utokyo-ipp.github.io/index.html>


でもそれだけではない

Numpyの学び方 <https://numpy.org/ja/learn/>