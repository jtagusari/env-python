import numpy as np
def arange_square_matrix(n):
  # ln = []
  # for i in range(n):
  #   ln.append(np.arange(i, i + n))
  
  ln = [np.arange(i, n+i) for i in range(n)]
  
  
  return np.array(ln)

a = arange_square_matrix(3)
b = arange_square_matrix(3)

x = 1