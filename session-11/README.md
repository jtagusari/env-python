# 第11回

## Numpy筋トレ

問題を解いて基礎的な使いかたを身に付ける。
ある程度使える様にはしておきたい。

<https://github.com/rougier/numpy-100>

## Numpyだけだと面白くない

可視化`matplotlib`や科学計算`scipy`も使ってみる。

たとえば，

露木 誠, 小田切 篤, 大谷 弘喜. (2020). パーフェクト Python [改訂2版] (PERFECT SERIES 5) 第16章. <https://u.pcloud.link/publink/show?code=kZHPgu0Z5yDjEMyiTKmProG3Uvx3ByKHMv47>

すべての基本は写経。

## 機械学習

ナイーブベイズ程度ならもうできる？

神嶌 敏弘（産業技術総合研究所）. 機械学習の Python との出会い. <https://www.kamishima.net/mlmpyja/>　および <https://github.com/tkamishima/mlmpy>