import numpy as np

class NaiveBayes1(object):
  def __init__(self):
    self.pY_ = None
    self.pXgY_ = None

  def fit(self, X, y):
    n_samples = X.shape[0] # データ数
    n_features = X.shape[1] # 特徴量の数
    
    n_classes = 2
    n_fvalues = 2
    
    nY = np.zeros(n_classes, dtype = int) # クラスごとのデータ数
    for i in range(n_samples):
      nY[y[i]] += 1
      
    self.pY_ = np.empty(n_classes, dtype = float)
    for i in range(n_classes):
      self.pY_[i] = nY[i] / n_samples
    
    nXY = np.zeros((n_features, n_fvalues, n_classes), dtype = int)
    for i in range(n_samples):
      for j in range(n_features):
        nXY[j, X[i,j], y[i]] += 1
  
    self.pXgY_ = np.empty((n_features, n_fvalues, n_classes), dtype = float)
    for i in range(n_features):
      for j in range(n_fvalues):
        for k in range(n_classes):
          self.pXgY_[i,j,k] = nXY[i,j,k] / nY[k]
  
  def predict(self, X):
    n_samples = X.shape[0] # データ数
    n_features = X.shape[1] # 特徴量の数
    y = np.empty(n_samples, dtype = int) # 出力の初期化
    
    for i in range(n_samples):
      xi = X[i, :]
      logpXY = np.log(self.pY_)
      
      for j in range(n_features):
        logpXY += np.log(self.pXgY_[j, xi[j], :])
      y[i] = np.argmax(logpXY)
    
    return y
  

data = np.genfromtxt("session-12/vote_filled.tsv", dtype = int)

X = data[:, :-1]
y = data[:, -1]

clr = NaiveBayes1()
clr.fit(X, y)

predict_y = clr.predict(X[:10])

for i in range(10):
  print(i, predict_y[i], y[i])