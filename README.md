# python勉強会

2024年春のpython勉強会の資料

## 参加者

- 8講座の人々
- 10講座の人々
- 外部スペシャルゲスト
  - 中村さん（Python札幌代表）
  - 小田さん

## 勉強のスタート位置

- 大学の授業でやったくらいの状態
- 研究でちょっと使ったくらいの状態

## 動機

- 研究で使えるようになりたい
- ちょっとしたデータ分析をできるようになりたい
- IT系企業に就職するので，基礎を身に付けておきたい

## これまでの進捗など

- 動画ファイル置き場：<https://drive.google.com/drive/folders/11IpW5NCPSJtL0NN-7nJJIRhqnXNXSIel?usp=sharing>

1. Python環境構築（2024-05-01 田鎖）
1. GitHub Copilot・オブジェクト指向とは（2024-05-08 田鎖）
1. Python入門（2024-05-15 中村）
1. Python入門2（2024-05-22 中村）
1. 時系列解析（2024-05-29 中村）
1. Python文法：標準ライブラリ（2024-06-05 田鎖）
1. Python文法：東大資料（変数と関数の基礎，文字列）（2024-06-12 田鎖）
1. Python文法：東大資料（リスト，タプル，辞書，条件分岐）（2024-06-19 田鎖）
1. Python文法：東大資料（繰り返し）（2024-06-26 田鎖）
1. Numpy：基礎／Quarto（2024-07-03 田鎖）
1. Numpy/matplotlib/scipy：科学技術計算キソ（2024-07-10 田鎖）
1. Numpy：応用／ナイーブベイズ（2024-07-18 田鎖）
1. Pandas：基礎（2024-07-31 田鎖）
1. 数値計算あれこれ（2024-08-07 中村）

## リンク，資料など

- 東京大学 数理・情報教育研究センター. (2023). Pythonプログラミング入門：<https://utokyo-ipp.github.io/index.html>

- Nicolas P. Rougier. 100 numpy exercises. <https://github.com/rougier/numpy-100>
- アレン・B・ダウニー. 相川利樹 訳. (2018). Think Python：コンピュータサイエンティストのように考えてみよう第２版 <https://cauldron.sakura.ne.jp/thinkpython/thinkpython/ThinkPython2.pdf>
- MIT. (2024). Introduction to Deep Learning. <http://introtodeeplearning.com/>
- 鈴木大慈. (2023). 深層学習の数理. <https://ibis.t.u-tokyo.ac.jp/suzuki/lecture/2023/TohokuUniv/%E6%9D%B1%E5%8C%97%E5%A4%A7%E5%AD%A62023.pdf>
- Alfredo Canziani. (2021). NYU Deep Learning Spring 2021 (NYU-DLSP21). <https://github.com/Atcold/NYU-DLSP21>
- Bhiksha Raj and Rita Singh. (2021). 11-785 Introduction to Deep Learning <https://deeplearning.cs.cmu.edu/F21/index.html>

## 今後の予定

- データ分析(pandas)
- データベースのはなし
- DL（PyTorch），機械学習の数学
