# Pythonコーディング規約・言語リファレンス・標準ライブラリ

## Python Enhancement Proposals

Python Enhancement Proposals <https://peps.python.org/pep-0000/>

- PEP 8: Style Guide for Python Code <https://pep8-ja.readthedocs.io/ja/latest/>
- PEP 20: The Zen of Python <https://peps.python.org/pep-0020/>
- コードは書くよりも読まれることの方が多い

### PEP 20: The Zen of Python

<https://qiita.com/IshitaTakeshi/items/e4145921c8dbf7ba57ef>

```
Beautiful is better than ugly.
醜いより美しいほうがいい。

Explicit is better than implicit.
暗示するより明示するほうがいい。

Simple is better than complex.
複雑であるよりは平易であるほうがいい。

Complex is better than complicated.
それでも、込み入っているよりは複雑であるほうがまし。

Flat is better than nested.
ネストは浅いほうがいい。

Sparse is better than dense.
密集しているよりは隙間があるほうがいい。

Readability counts.
読みやすいことは善である。

Special cases aren't special enough to break the rules.
特殊であることはルールを破る理由にならない。

Although practicality beats purity.
しかし、実用性を求めると純粋さが失われることがある。

Errors should never pass silently.
エラーは隠すな、無視するな。

Unless explicitly silenced.
ただし、わざと隠されているのなら見逃せ。

In the face of ambiguity, refuse the temptation to guess.
曖昧なものに出逢ったら、その意味を適当に推測してはいけない。

There should be one-- and preferably only one --obvious way to do it.
何かいいやり方があるはずだ。誰が見ても明らかな、たったひとつのやり方が。

Although that way may not be obvious at first unless you're Dutch.
そのやり方は一目見ただけではわかりにくいかもしれない。オランダ人にだけわかりやすいなんてこともあるかもしれない。

Now is better than never.
ずっとやらないでいるよりは、今やれ。

Although never is often better than *right* now.
でも、今"すぐ"にやるよりはやらないほうがマシなことが多い。

If the implementation is hard to explain, it's a bad idea.
コードの内容を説明するのが難しいのなら、それは悪い実装である。

If the implementation is easy to explain, it may be a good idea.
コードの内容を容易に説明できるのなら、おそらくそれはよい実装である。

Namespaces are one honking great idea -- let's do more of those!
名前空間は優れたアイデアであるため、積極的に利用すべきである。
```

### PEP 8: Style Guide for Python Code

<https://pep8-ja.readthedocs.io/ja/latest/>

- コードのレイアウト
  - 1レベルインデントするごとに、スペースを4つ使いましょう。
- コメント
  - コードと矛盾するコメントは、コメントしないことよりタチが悪いです。コードを変更した時は、コメントを最新にすることをいつも優先させてください！
― 命名規約
  - 関数の名前は小文字のみにすべきです。また、読みやすくするために、必要に応じて単語をアンダースコアで区切るべきです。
  - 全ての定数は大文字で書き、単語をアンダースコアで区切ります。例として `MAX_OVERFLOW` や `TOTAL` があります。

その他もろもろ

## 言語リファレンス

Python 言語リファレンス <https://docs.python.org/ja/3/reference/index.html>

### 字句解析: リテラル（2.4）

#### 文字列・f文字列

`[stringprefix](shortstring | longstring)`

- `[stringprefix]`: `r`, `f`などのプレフィックス。なくてもよい。
- `[shortstring]`: `'` もしくは `"` で囲まれた文字列。ただし`\`は単体で使われることはない。
- `[longstring]`: `'''` もしくは `"""` で囲まれた文字列。ただし`\`は単体で使われることはない。

例

- `f'abc{x}'`（参照：`f文字列`）
- `'abc'`
- `"abc"`
- `'''abc'''`
- `"Desktop\\hoge\\test.py"`

`\`はエスケープシーケンスであり，続く文字に応じて特別な意味を持つ。たとえば，`\n`は改行，`\\`はバックスラッシュ。

#### 整数

`decinteger | bininteger | octinteger | hexinteger`

- `decinteger`: 10進数。
- `bininteger`: 2進数。`0b`で始まり，0-1の数値が続く。間に`_`を入れても良い。
- `octinteger`: 8進数。`0o`で始まり，0-7の数値が続く。間に`_`を入れても良い。
- `hexinteger`: 16進数。`0x`で始まり，0-9, A-Fの数値が続く。間に`_`を入れても良い。

例

- `123`
- `123_456_789`
- `0b1010`
- `0o777`
- `0xdeadbeef`

#### 浮動小数点数

`pointfloat | exponentfloat`

- `pointfloat`: 小数点表記。ただし，`.01`や`241.`などの表記も許され，0が省略されているものとして扱われる。間に`_`を入れても良い。
- `exponentfloat`: 指数表記。`1e-3`は`0.001`と等価。間に`_`を入れても良い。

例

- `3.14`
- `10.`
- `1_000.0`
- `1.0e3`

#### 虚数

`(floatnumber | digitpart) ("j" | "J")`

- `floatnumber`: 浮動小数点
- `digitpart`: 整数
- `j`または`J`で終わる

### データモデル：オブジェクト，値，および型（3.1）

- Python における オブジェクト (object) とは、データを抽象的に表したもの
- Python プログラムにおけるデータは全て、オブジェクトまたはオブジェクト間の関係として表される

```
オブジェクトによっては 値 を変更することが可能です。値を変更できるオブジェクトのことを mutable と呼びます。生成後に値を変更できないオブジェクトのことを immutable と呼びます。(mutable なオブジェクトへの参照を格納している immutableなコンテナオブジェクトの値は、その格納しているオブジェクトの値が変化した時に変化しますが、コンテナがどのオブジェクトを格納しているのかが変化しないのであれば immutable だと考えることができます。したがって、immutable かどうかは値が変更可能かどうかと完全に一致するわけではありません) オブジェクトが mutable かどうかはその型によって決まります。例えば、数値型、文字列型とタプル型のインスタンスは immutable で、dict や list は mutable です。
```

### 実行モデル：名前付けと束縛（4.2）

変数には，プログラム全体で有効なグローバル変数と，クラスや関数の定義内でのみ有効なローカル変数がある。

プログラムテキスト中に変数名が出現するたび，その名前が使われている最も内側のコードブロックで名前の参照が行われる。（最も内側のブロック中で作成された束縛 (binding) を使って名前の参照が行われる。）

変数が参照可能な有効範囲のことをスコープ (scope) と呼ぶ。

あるコードブロック内で参照できるスコープ全ての集合は，ブロックの環境 (environment) と呼ばれる。

```python
x = 10 # グローバル変数
def f():
    x = 20 # ローカル変数
    print(x) # ローカル変数が参照される
f()
print(x) # グローバル変数が参照される
```

## Python標準ライブラリ

Python 標準ライブラリ <https://docs.python.org/ja/3/library/index.html>

## 人の作った資料で勉強する

- Pythonプログラミング入門（東京大学数値・情報教育研究センター） <https://utokyo-ipp.github.io/index.html>
- Python学習教材（筑波大学システム情報系　三谷純） <https://mitani.cs.tsukuba.ac.jp/book_support/python/python_slides.pdf>