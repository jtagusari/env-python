# Pythonことはじめ

## 環境構築

Pythonを使えるようにするまでが大変。
なお，OSはWindows 11を想定している。

基本的には，Python.jpが示している方法で進める。<https://www.python.jp/python_vscode/windows/setup/index.html>

### Python

Python.jpの通り。
ただ，いまのところ，Windows Store版のPythonでも問題はなさそう。

インストールするPythonのバージョンは注意。
何も制約がなければ最新の安定版を使えば良いが，グループで解析をする場合には，バージョンを合わせておかないと「大変なこと」になる。

ところで，ソフトウェアのバージョンは，「セマンティックバージョニング」というルールに基づき，`MAJOR.MINOR.PATCH`の形式で表現されている。
`MAJOR`は大きな変更があったときに上げる。`MINOR`は機能追加があったときに上げる。`PATCH`はバグ修正があったときに上げる。
（特に，Pythonにおいては`MAJOR`は破壊的なバージョンアップであるようだ）

### VSCode

Python.jpの通り。
VSCodeは，`GitHub Copilot`も使えて便利。

インストール時には，「右クリックメニューに追加」に忘れずにチェックを入れておく。

インストールができたら，VSCodeを起動し，左側の拡張機能のアイコンをクリックし，少なくとも以下の拡張機能をインストールする。

- Japanese Language Pack for Visual Studio Code
- Python
- GitHub Copilot (GitHubの設定をしないと使えないが)

左下歯車マークから`settings.json`を開き，`python.defaultintepreterPath`（デフォルトのPython実行環境）を設定するのを忘れずに。

VSCode上で，PowerShellなどのターミナルが開ける。
ターミナル上から，Pythonの様々な操作が可能。
`python`と入力すると，Pathが通っていれば，Python実行環境が起動する。`quit()`で終了する。

どのPythonが実行されているかを確認するには，`gcm python | fl`とする。

### GitHub Copilot

GitHub Copilotは，AIによるコード補完機能を提供する拡張機能である。
（この文章を書くのにも，GitHub Copilotを使っている。）
VSCodeの拡張機能からインストールすることができるが，使うためにはGitHubのアカウントが必要である。
そこで，まず，GitHub <https://github.com> のアカウントを作成する。

教員や学生は，GitHub Educationのプランを使うことができる。
<https://education.github.com> から登録することで，無制限のプライベートリポジトリ，GitHub Copilotの利用，GitHub Actionsの利用などができる。
（この文章もGitHub Copilotが書いているので，真偽は不明であるが……）

GitHub Educationに登録するためには，学生証や教員証の提出が必要である。
学生証や教員証をスキャンして，アップロードすることで登録が完了する。
このとき，学生証や教員証の情報について英語でメモを付けておかないといけない。
また，GitHubアカウントのProfileに学生証や教員証の情報が記載されていることが必要である。
なお，登録には数日かかることがあるので，早めに登録しておくことをお勧めする。
（詳細なノウハウは親切な人が書いた記事が参考になる<https://qiita.com/SNQ-2001/items/796dc5e794ac3f57a945>。）

GitHubのアカウントが作成できたら，VSCodeの左下の「アカウント」をクリックし，GitHubアカウントでログインする。

GitHub Copilotの使いかたについては，わかりやすくまとめてくれている人が多くいる。たとえば，<https://zenn.dev/umi_mori/books/ai-native-programming>など。
ただし，色々な機能を使わずとも，コードの補完だけで十分に使用価値がある。

たとえば，`sample.py`では，実際に入力したのは以下の2行のみで，後はGitHub Copilotが補完してくれた。

```python
# 10000未満の素数を表示するプログラム

def
```


GitHub Copilotは，あくまでもプログラミングの補助ツールと位置づけられており，chatにプログラミング以外の質問をすると，回答を拒否されることがある。
しかし，マークダウン（`.md`），LaTeX（`.tex`）などのファイルであれば，文章の補完も行ってくれる！！
Wordで文章を書くよりも，VSCodeで文章を書いた方が，GitHub Copilotの補完が効いて，効率が良いことが多い（特に英語の文章を書くとき）。

`copilot-meros.tex`は，`\begin{document}`と`\end{document}`で囲われたLaTeX文書であり，以下の文に続けて，Copilotに分を書かせた。

```LaTeX
\begin{document}

メロスは激怒した。必ず、かの邪智暴虐の王を除かなければならぬと決意した。

\end{document}
```

## 独学の方法

ある程度の基礎を身に付ける方法として，paiza <https://paiza.jp/> を挙げておく。
アカウントを作れば，「paizaラーニング」や「スキルチェック問題」が無料で利用可能であり，Pythonの基礎を身に付けるのに役立つ。
（ただし，ある一定レベルを超えると，「アルゴリズム」の問題になり，Pythonの勉強にはあまり役立たない）

せっかくPythonの環境を構築したところだが，paizaはブラウザ上でPythonを実行できるので，PCにPythonをインストールしていない人でも利用できる。


## このあと何をする？

様々な操作の基本として，オブジェクト指向プログラミングの基礎を学ぶ。