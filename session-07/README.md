# Python文法ゴリゴリ

## 人の資料で勉強する

とりあえず，イテレータとか繰り返しが出てくる前までの部分について，人の資料を使って勉強する

- Python学習教材（筑波大学システム情報系　三谷純） <https://mitani.cs.tsukuba.ac.jp/book_support/python/python_slides.pdf>
- Pythonプログラミング入門（東京大学数値・情報教育研究センター） <https://utokyo-ipp.github.io/index.html>
  - 1-2. 変数と関数の基礎
  - 1-3. 論理・比較演算と条件分岐の基礎
  - 1-4. テストとデバッグ
  - 2-1. 文字列
  - 2-2. リスト
  - 2-3. 条件分岐
  - 3-1. 辞書

## 繰り返し

イテラブル／イテレータ／ジェネレータ
